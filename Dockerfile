FROM debian:11.6

WORKDIR /usr/src/app

COPY . .

RUN apt-get update -y && apt-get install -y python3-pip

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

CMD [ "python3", "app.py"]
